package com.company.data.database;

import com.company.functional.Depositor;
import com.company.model.FinancialOrganization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class DepositDataBase {
    private ArrayList<Depositor> depositors;

    public DepositDataBase(ArrayList<FinancialOrganization> organizations) {
        this.depositors = findDepositors(organizations);
    }

    /**
     * Про классы собранные в этом пакете не беду много рассказывать
     * так как они являются больше утилитными чем классами описывающими
     * принцыппы ООП
     *
     * Оращу внимание только на коллекцию "ArrayList<Depositor> depositors".
     * Благодаря наследованию данная колекция может хранить в себе
     * ссылку на любой объект дочернего класса от Depositor
     * **/

    public ArrayList<Depositor> getDepositors() {
        return depositors;
    }

    private ArrayList<Depositor> findDepositors(ArrayList<FinancialOrganization> organizations) {
        ArrayList<Depositor> depositors = new ArrayList<>();
        for (FinancialOrganization organization : organizations) {
            if (organization instanceof Depositor) {
                depositors.add((Depositor) organization);
            }
        }
        return depositors;
    }

    public void sortAnnualInterest() {
        Collections.sort(depositors,
                (depositor1, depositor2) -> Float.compare(depositor2.getDepositAnnualInterest(),depositor1.getDepositAnnualInterest()));
    }

    public void checkCanItGiveDeposit(int months) {
        Iterator<Depositor> iterator = depositors.iterator();
        while (iterator.hasNext()){
            if (!iterator.next().canGiveDeposit(months)){
                iterator.remove();
            }
        }
    }

}
