package com.company.model;

public abstract class FinancialOrganization {
    /**
     * Данный класс являеться отличным примером Наследования.
     * Он описывает общие свойства для ряда классов собранных в пакете "organizations"
     *
     * Модификатор доступа "protected" является частным случаем инкапсуляции.
     * Он ограничивает доступ к переменной в приделах класса
     * носителя а также до классов наследников (если такие имеются)
     * **/
    protected String name;
    protected String address;

    public FinancialOrganization(String name, String address) {
        this.name = name;
        this.address = address;
    }


    public abstract String getInfo();
}
