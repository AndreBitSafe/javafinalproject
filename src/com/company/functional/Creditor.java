package com.company.functional;

import com.company.model.FinancialOrganization;

/**
 * Данный интерфейс описывает поведение, каторое,
 * имплементирующий его клас должен будет реализовать по своему
 * Данный энтерфейс является ярким примером наследования, а
 * его реализованные методы в классах имплементаторах, выражают
 * полеморфизм
 * **/

public interface Creditor {
    int takeCredit(int money, int months);

    boolean canGiveCredit(int amount);

    float getCreditAnnualInterest();

    String getCreditInfo();
}
