package com.company.functional;

import com.company.model.FinancialOrganization;
/**
 * Данный интерфейс описывает поведение, каторое,
 * имплементирующий его клас должен будет реализовать по своему
 * Данный энтерфейс является ярким примером наследования, а
 * его реализованные методы в классах имплементаторах, выражают
 * полеморфизм
 * **/

public interface Depositor {
    int openDeposit(int money, int monthNum);

    boolean canGiveDeposit(int monthNum);

    String getDepositInfo();

    float getDepositAnnualInterest();
}
