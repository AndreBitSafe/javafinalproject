package com.company.functional;

import com.company.model.Currency;

/**
 * Данный интерфейс описывает поведение, каторое,
 * имплементирующий его клас должен будет реализовать по своему
 * Данный энтерфейс является ярким примером наследования, а
 * его реализованные методы в классах имплементаторах, выражают
 * полеморфизм
 * **/

public interface Converter {

    float convert(int money, ConvertAction action, String currencyName);

    Currency getCurrency(String name);

    boolean canConvert(int amount, String currName);

    enum ConvertAction {
        BAY, SELL
    }
}
